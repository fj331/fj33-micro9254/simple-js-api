import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { AddOperatorDto } from './dto/add.operator.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }


  @Get('health')
  health() {
    return { status: 'UP' };
  }

  @Post('add')
  add(@Body() addDTO: AddOperatorDto) {
    const {a, b} = addDTO

    console.log(`Add(${a}, ${b})`)

    return { result: a + b }
  }
}
